# MySQL Workbench汉化

#### MySQL Workbench汉化介绍
打开MySQL Workbench的文件所在位置，然后进入 **data** 目录中，找到 **main_menu.xml** 文件 <br/>
我的是window电脑，默认路径是：`C:\Program Files\MySQL\MySQL Workbench 8.0 CE\data`<br/>
我的路径仅供参考，目的是让大家找到data目录中的main_menu.xml文件<br/>
然后将我项目中的main_menu.xml文件打开，然后复制所有代码 替换掉原文件main_menu中的内容，保存，然后重启MySQL Workbench就可以了

#### 其他
有其他问题欢迎留言 :bowtie: 